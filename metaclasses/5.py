class Meta(type):
    instances = {}

    def __call__(cls, *args, **kwargs):
        if cls not in Meta.instances:
            Meta.instances.update(
                {
                    cls: super().__call__(*args, **kwargs)
                }
            )

        return Meta.instances[cls]


class Example(metaclass=Meta):
    def __new__(cls, *args, **kwargs):
        print("Example __new__")
        return super().__new__(cls, *args, **kwargs)

    def __init__(self):
        print("example")


class Example2(metaclass=Meta):
    def __new__(cls, *args, **kwargs):
        print("Example2 __new__")
        return super().__new__(cls, *args, **kwargs)

    def __init__(self):
        print("example")


if __name__ == '__main__':
    delimiter = "-" * 50
    print(delimiter)
    a = Example()
    b = Example()
    assert a is b

    c = Example2()
    e = Example2()
    assert c is e

    assert a is not c
