class Meta(type):
    def __init__(cls, name, base, attrs):
        print(f"Meta __init__ arguments {cls} {base} {attrs}")
        super().__init__(name, base, attrs)

    def __new__(cls, name, base, attrs):
        print(f"Meta __new__ arguments: {cls} {base} {attrs}")
        self = super().__new__(cls, name, base, attrs)
        print(f"Meta __new__ created obj: {self}")
        return self

    def __call__(cls, *args):
        print(f"Meta __call__ {cls} {args}")
        call__ = super().__call__(*args)
        return call__


class Example(metaclass=Meta):

    def __init__(self, test_field):
        self.field = test_field
        print(f"Example __init__ with field {test_field}")


if __name__ == '__main__':
    delimiter = "-" * 20
    a = Example(10)

    print(delimiter)
    b = Example(10)