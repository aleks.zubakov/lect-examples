class Meta(type):
    X = [1, 2, 3]

    def some_method(cls):
        return "foobar"

    @classmethod
    def bar_only(cls):
        return "bar"


class Example(metaclass=Meta):
    def __init__(self):
        pass


if __name__ == '__main__':

    print(Example.some_method)
    print(Example.X)
    print(Example.bar_only)