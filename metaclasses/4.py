from inspect import signature


class Meta(type):
    def __init__(cls, name, base, attrs):
        print(f"Meta __init__ arguments {cls} {base} {attrs} ")
        super().__init__(name, base, attrs)

    def __new__(cls, name, base, attrs):
        print(f"Meta __new__ arguments: {cls} {base} {attrs}")
        self = super().__new__(cls, name, base, attrs)
        print(f"Meta __new__ created obj: {self}")
        return self

    @staticmethod
    def __init_filled(cls, *args):
        param_num = len(signature(cls.__init__).parameters)
        arg_num = len(args)
        args_list = list(args)
        if arg_num < param_num:
            args_list.extend([None] * (param_num - arg_num - 1))

        return super(Meta, cls).__call__(*args_list)

    def __call__(cls, *args):
        return Meta.__init_filled(cls, *args)


class Example(metaclass=Meta):
    def __init__(self, a, b, c):
        print(f"Example {a} {b} {c}")


if __name__ == '__main__':
    delimiter = "-" * 20
    print(delimiter)
    print(Example(10, 20, 30))

    print(delimiter)
    print(Example(30))
