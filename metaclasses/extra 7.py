class Bar():
    def __init__(self):
        print(f"Bar init")

    def __call__(self):
        print("Bar call")


class Meta(type):
    def __init__(cls, name, base, attrs):
        print(f"Meta __init__ arguments {cls} {base} {attrs}")
        super().__init__(name, base, attrs)

    def __new__(cls, name, base, attrs):
        print(f"Meta __new__ arguments: {cls} {base} {attrs}")
        self = Bar()
        print(f"Meta __new__ created obj: {self}")
        return self

    def __call__(cls, *args):
        print(f"Meta __call__ {cls} {args}")
        call__ = super().__call__(*args)
        return call__


class Foo(metaclass=Meta):
    def __init__(self):
        print(f"Foo init")


if __name__ == '__main__':
    delimiter = "-" * 20
    print(delimiter)
    a = Foo()

    print(delimiter)
    b = Foo()
