from collections import OrderedDict


class Meta(type):

    def __new__(cls, name, base, attrs):
        print(f'Attr type: {type(attrs)}')
        self = super().__new__(cls, name, base, attrs)
        return self

    @classmethod
    def __prepare__(mcs, name, bases):
        return OrderedDict()


class Example(metaclass=Meta):
    def __init__(self, a, b, c):
        print(f"Example {a} {b} {c}")


if __name__ == '__main__':
    delimiter = "-" * 20
    print(delimiter)
    print(Example(10, 20, 30))

    print(delimiter)
    print(Example(10, 30, 40))

    print(delimiter)
    print(Example(10, 20, 30))
