class Meta1(type):
    pass


class Meta2(type, metaclass=Meta1):
    pass


class Foo1(metaclass=Meta1):
    pass


class Foo2(metaclass=Meta2):
    pass


# class Boo(Foo1, Foo2):
#     pass


# if __name__ == '__main__':
#     print(Boo())

class A:
    def _foo(self):
        print("aa")

    def __boo(self):
        print("aa")


if __name__ == '__main__':
    print(dir(A()))
